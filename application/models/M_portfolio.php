<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class M_portfolio extends CI_Model
    {
        private $_table = 'portfolio';

        public $id_portfolio;
        public $gambar_portfolio = "default.jpg";
        public $keterangan_portfolio;

        public function rules(){
            return [
                ['field'  => 'gambar_portfolio',
                'label'=> 'Gambar',
                'rules' => 'required'],

                ['field'  => 'keterangan_portfolio',
                'label'=> 'Keterangan',
                'rules' => 'required']
            ];
        }

        public function getall()
        {
            return $this->db->get($this->_table)->result();
        }

        public function getbyid()
        {
            return $this->db->get_where($this->_table, ["id_portfolio" => $id_portfolio])->row();
        }

        public function save($data)
        {
            // $post = $this->input->post();
            // // $this->id_portfolio = uniqid();
            // $this->gambar_portfolio = $post['gambar_portfolio'];
            // $this->deskripsi_portfolio = $post['deskripsi_portfolio'];
            // $this->db->insert($this->_table,$this);
            $this->db->insert('portfolio',$data);
        }

        public function update()
        {
            $post = $this->input->post();
            $this->id_portfolio = $post['id_portfolio'];
            $this->gambar_portfolio = $post['gambar_portfolio'];
            $this->deskripsi_portfolio = $post['deskripsi_portfolio'];
            $this->db->update($this->_table,$this, array('id_portfolio' => $post['id_portfolio']));
        }

        public function delete()
        {
            return $this->db->delete($this->_table, array("id_portfolio" => $id));
        }
    }
    
?>