<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="<?= site_url();?>/welcome">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li><a href="<?php echo site_url();?>/portfolio"><i class="fa fa-image"></i> <span>Portfolio</span></a></li>
      <li><a href=""><i class="fa fa-book"></i> <span>Blogs</span></a></li>
      <li><a href="#"><i class="fa fa-envelope-o"></i> <span>Message</span></a></li>
      <li><a href="<?= base_url().'Dashboard/logout'?>"><i class="fa fa-log-out"></i> <span>Log Out</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>