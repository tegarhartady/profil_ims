<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="button">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
                Add Portfolio
          </button>
          </div>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Gambar</th>
                  <th>Deskripsi</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach($portfolio as $p):?>
                    <tr>
                      <td><img src="<?= base_url('upload/product/'. $p->gambar_portfolio);?>" width=""></td>
                      <td><?= $p->keterangan_portfolio;?></td>
                    </tr>
                  <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!-- Modal Add -->
        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <?php echo $this->session->flashdata('success');?>
              <div class="modal-body">
                <form action="<?php echo site_url()?>/portfolio/save" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="">Gambar</label>
                    <input type="file" name="gambar_portfolio" class="form-control-file <?= form_error('gambar_portfolio') ? 'is-invalid' : '' ?>">
                    <div class="invalid-feedback">
                      <?= form_error('gambar_portfolio')?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Deskripsi</label>
                    <textarea name="deskripsi_portfolio" class="form-control <?= form_error('desripsi_portfolio')?>" cols="30" rows="10" placeholder="Tambahkan Deskripsi"></textarea>
                    <div class="invalid-feedback">
                      <?= form_error('deskripsi_portfolio')?>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-success" name="button" value="Save">
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>
      <!-- /.row -->
    </section>