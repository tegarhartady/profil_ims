<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('Admin');
    }
	public function index()
	{
		if($this->Admin->logged_id())
		{
			$data['page'] = 'index';
			$data['content'] = 'index';
			$data['title'] = 'Dashboard';
			$this->load->view("index",$data);			

		}else{

			//jika session belum terdaftar, maka redirect ke halaman login
			redirect("login");

		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

}
