<?php
    defined("BASEPATH") OR exit('No direct script access allowd');
    
    class portfolio extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('M_portfolio');
            $this->load->library('form_validation','upload');
        }

        public function index()
        {
            $data['page'] = 'Portfolio';
            $data['title'] = 'Portfolio';
            $data['content'] = 'admin/portfolio';
            $data['portfolio'] = $this->M_portfolio->getall();
            $this->load->view('index',$data);
        }

        public function save()
        {
            // $porduct = $this->M_portfolio;
            // $validation = $this->form_validation;
            // $validation->set_rules($porduct->rules());
            // if ($validation->run()) {
            //     $porduct->save();
            //     $this->session->set_flashdata('success','Berhasil Disimpan');
            // }
            $data = array('gambar_portfolio' => $this->input->post('gambar_portfolio'),
            'keterangan_portfolio' => $this->input->post('deskripsi_portfolio'));
            $this->M_portfolio->save($data);
            redirect('portfolio');
            
        }

        public function update($id = null)
        {
            if (!isset($id)) redirect('admin/portfolio');

            $product = $this->M_portfolio;
            $validation = $this->form_validation;
            $validation->set_rules($product->rules());

            if ($validation->run()) {
                $product->update();
                $this->session->set_flashdata('success','Berhasil Disimpan');

            }

            $data['product'] = $product->getbyid($id);
            if (!$data['product']) show_404();

            $this->load->view('admin/product/edit_form',$data);
        }

        public function delete($id=null)
        {
            if(!isset($id)) show_404();

            if ($this->M_portfolio->delete($id)) {
                redirect(site_url('admin/portfolio'));
            }
        }
    }
    
?>